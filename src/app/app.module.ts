import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { SliderPage } from '../pages/slider/slider';
import { LocateMePage } from '../pages/locate-me/locate-me';
import { MmoneyPage } from '../pages/mmoney/mmoney';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { ApisProvider,CartService} from '../providers/apis/apis';
import { GroceryPage } from '../pages/grocery/grocery';
import { VerifyPage } from '../pages/verify/verify';
import { CartsPage } from '../pages/carts/carts';
import { VisaPage } from '../pages/visa/visa';
import { OrderModalPage } from '../pages/order-modal/order-modal';
import { OrderPage } from '../pages/order/order';
import { PaymentPage } from '../pages/payment/payment';
import { OrderHistoryPage } from '../pages/order-history/order-history';
import { SignupPage } from '../pages/signup/signup';
import { ProdDetailsPage } from '../pages/prod-details/prod-details';
import { DeliveryDetailsPage } from '../pages/delivery-details/delivery-details';
import { PaymentProcessedPage } from '../pages/payment-processed/payment-processed';

//import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SliderPage,
    LocateMePage,
    GroceryPage,
    ProdDetailsPage,
    CartsPage,
    OrderPage,
    SignupPage,
    OrderHistoryPage,
    PaymentPage,
    MmoneyPage,
    PaymentProcessedPage,
    VerifyPage,
    DeliveryDetailsPage,
    VisaPage,
    OrderModalPage
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    
  ],


  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SliderPage,
    LocateMePage,
    GroceryPage,
    ProdDetailsPage,
    CartsPage,
    OrderPage,
    SignupPage,
    OrderHistoryPage,
    PaymentPage,
    MmoneyPage,
    PaymentProcessedPage,
    VerifyPage,
    DeliveryDetailsPage,
    VisaPage,
    OrderModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApisProvider,
    CartService,
   
  ]
})
export class AppModule {}
