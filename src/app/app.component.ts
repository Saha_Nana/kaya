import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CartsPage } from '../pages/carts/carts';
 import { LoginPage } from '../pages/login/login';
import { SliderPage } from '../pages/slider/slider';
import { OrderHistoryPage } from '../pages/order-history/order-history';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SliderPage;

  pages: Array<{ title: string, component: any, icon: string }>;
  pages2: Array<{ title: string, component: any, icon: string }>;
  pages3: Array<{ title: string, component: any, icon: string }>;
  pages4: Array<{ title: string, component: any, icon: string }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'My Shops', component: HomePage, icon: "md-basket" },
      { title: 'My Cart', component: CartsPage, icon: "md-cart" },
      { title: 'My Orders', component: OrderHistoryPage, icon: "md-checkbox" },
      { title: 'My Payments', component: ListPage, icon: "md-card" },

      

    ];

    this.pages2 = [

      { title: 'Favorite List', component: ListPage, icon: "md-heart" },
      { title: 'Settings', component: ListPage, icon: "md-settings" },

    ];

    this.pages3 = [
      { title: 'Help', component: ListPage, icon: "md-help-circle" },
      { title: 'Rate Us', component: ListPage, icon: "md-star" },
      { title: 'Share', component: ListPage, icon: "md-git-branch" },
      { title: 'About Kaya', component: ListPage, icon: "md-information-circle" },
      { title: 'Sign Out', component: ListPage, icon: "md-power" },

    ];

  

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
