// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ApisProvider {

  constructor(public http: Http) {
    console.log('Hello ApisProvider Provider');
  }



signup_url =  'http://67.205.74.208:8732/account_creation';
login_url =  'http://67.205.74.208:8732/account_access';

verify_url = 'http://67.205.74.208:8732/account_activation'
shops_url = 'http://67.205.74.208:8732/fetch_grocery_shops';
prod_url = 'http://67.205.74.208:8732/grocery_store_product_types/';
prod_list_url= 'http://67.205.74.208:8732/fetch_products_by_product_id/';
process_orders_url = 'http://67.205.74.208:8732/process_orders';
display_orders_url = 'http://67.205.74.208:8732/displayOrderDetails'
order_history_url = 'http://67.205.74.208:8732/displayOrders'

  fetch_shops() {
  return new Promise((resolve, reject) => {
    this.http.get(this.shops_url)
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


  grocery_shops(data) {
  return new Promise((resolve, reject) => {
    this.http.get(this.prod_url+data,data)
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

 prod_details(data) {
  return new Promise((resolve, reject) => {
    this.http.get(this.prod_list_url+data,data)
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


signup(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.signup_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

verify(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.verify_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

login(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.login_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

display_orders(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.display_orders_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

order_history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.order_history_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


process_order(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.process_orders_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

}




export class MenuItem {
    id:number;
    product_id: string;
    product_name:string;
    product_price:number;
    discount:string;
    prod_logo_url: string;
    
    constructor(id:number,product_id: string, product_name : string, product_price:number, discount: string,prod_logo_url: string) {
        this.id = id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_price = product_price;
        this.discount = discount;
        this.prod_logo_url = prod_logo_url;
    }
}




export class CartItem { 
    id:number;
    product_id: string;
    product_name:string;
    product_price:number;
    discount:string;
    quantity:number;
    prod_logo_url: string;
    
    constructor(item : MenuItem, quantity:number) {
        this.id = item.id;
        this.product_id =item.product_id
        this.product_name = item.product_name;
        this.product_price = item.product_price;
        this.discount = item.discount;
        this.quantity = quantity;
        this.prod_logo_url = item.prod_logo_url;

         

    }
}


@Injectable()
export class CartService {
    list : Array<CartItem>; 
    
    constructor(){
        this.list = []
    }
    
    getAllCartItems(){
        return this.list;
    }
    
    addItem(product : MenuItem, quantity:number){
        
        var isExists : boolean = false;
        // var id = product.id;
        var id = product.product_id;
        
        
        for(var i = 0; i < this.list.length; i++){
            if(this.list[i].product_id == id){
                this.list[i].quantity += quantity;
                isExists = true;
                break;
            }
        }
        if(!isExists){
            this.list.push(new CartItem(product, quantity));
        }
    }

     quantityPlus(item){
        item.quantity += 1;
    }
    
      quantityMinus(item){
        item.quantity -= 1;
    }

        removeItemById(id){        
        for(var i = 0; i < this.list.length; i++){
            if(this.list[i].id == id){
                this.list.splice(i, 1);
                break; 
            }
        }        
    }


    getGrandTotal(): number{
        var amount = 0;
        for(var i = 0; i < this.list.length; i++){
            amount += (this.list[i].product_price * this.list[i].quantity);
        }
        return amount;
    }

  
   
}

