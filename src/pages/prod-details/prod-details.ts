import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApisProvider,CartService } from '../../providers/apis/apis';
import { CartsPage } from '../carts/carts';


import { ToastController, LoadingController, AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-prod-details',
  templateUrl: 'prod-details.html',
})
export class ProdDetailsPage {
    grocery: any;
    messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  prod_name: any;
  shop_name: any;
  from_menu: any = [];
  from_login: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  raw: any;
  count: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public apis: ApisProvider,public cartServ: CartService, public loadingCtrl: LoadingController) {
  

   console.log("-----------------------------------" )
   this.count = this.cartServ.getAllCartItems().length
   console.log("FIRST COUNT " + this.count )

 this.grocery = this.navParams.get('value')
 this.prod_name = this.navParams.get('prod_name')
  this.shop_name = this.navParams.get('shop_name')
    console.log("LETS SEE FROM HOME" +  this.grocery)


      this.raw = JSON.stringify(this.grocery);

    console.log('from ' +  this.grocery ); 
       console.log('RAW JSON' +  this.raw ); 
   
        
  
  this.params = {

   "product_id": this.grocery

  }  
 console.log('PARAMETERS' +  this.params );

   this.apis.prod_details(this.raw).then((result) => {

    console.log("RESULT FROM API " + result);
    console.log("RESULTS IN PARAMS " + this.apis.prod_details(this.raw));
    var body = result["_body"];
    console.log("LETS SEE BODY IN HOME " + body);
    body = JSON.parse(body);
    this.check = body

    console.log("LETS SEE THE JSON OBJECT1" + body);
     console.log("LETS SEE THE JSON OBJECT" + this.check);
    this.body = Array.of(this.check)

    console.log("LETS SEE THE JSON ARRAY with image url" + this.body);


    var desc = body["resp_desc"];
    var code = body["resp_code"];


    console.log(desc);
    console.log(code);

    this.messageList = desc;
    this.api_code = code;

    }, (err) => {


    console.log(err); 
  });



}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProdDetailsPage');
     this.count = this.cartServ.getAllCartItems().length
   console.log("FIRST COUNT " + this.count )
    
  }

cart(){

  this.navCtrl.push(CartsPage)
}

  addTocart(product){

    console.log("These are the product " + product); 
    let pro =  JSON.stringify(product);  
     console.log("NOw strign " + pro);      
        this.cartServ.addItem(product, 0);

        console.log("LETS SEE ADD ITEM" + this.cartServ.addItem(product, 1));
        console.log("LETS SEE ALL CART ITEM" +this.cartServ.getAllCartItems());
         console.log(this.cartServ.getAllCartItems());

         this.count = this.cartServ.getAllCartItems().length

         console.log("WHAT IS COUNT " + this.count)

          let string=  JSON.stringify(this.count);   

           console.log("STRINGFIY " +  string)

         

  }



}
