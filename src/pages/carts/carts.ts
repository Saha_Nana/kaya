import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApisProvider, CartService, CartItem } from '../../providers/apis/apis';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { OrderPage } from '../order/order';
import { DeliveryDetailsPage } from '../delivery-details/delivery-details';

import 'rxjs/add/operator/map';


@Component({
  selector: 'page-carts',
  templateUrl: 'carts.html',
})
export class CartsPage {
  cartList: Array<CartItem>;
  check: any;
  params: any;
  params2: any;
  select: any;
  orders: any;
  orderJson: any;
  order_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apis: ApisProvider, public cartServ: CartService, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    let loader = this.loadingCtrl.create({
      content: "Getting Cart Details ..."

    });

    loader.present();
    this.cartList = cartServ.getAllCartItems();
    this.select = JSON.stringify(this.cartList)
    loader.dismiss();
    console.log("LETS SEE THE CARTLIST " + this.cartList)
    console.log("LETS SEE THE CARTLIST STRINGIFIED " + this.select)
    console.log("LETS SEE THE CARTLIST STRINGIFIED with product name selected " + this.select[0].product_name)
    console.log("LETS SEE THE CARTLIST STRINGIFIED with product name selected " + this.cartList[0].product_name)
  }



  quantityAdd(item) {
    this.cartServ.quantityPlus(item);
  }

  quantityMinus(item) {
    if (item.quantity > 1) {
      this.cartServ.quantityMinus(item);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Quantity is 1, you cant reduce it, if you want to remove, please press remove button.',
        buttons: ['Ok']
      });
      alert.present();
    }
  }


  removeItemFromCart(item) {
    //this.cartService.removeItemById(item.id);

    let self = this;

    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure you want to remove food item from cart?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remove',
          handler: () => {
            console.log('Buy clicked');
            self.cartServ.removeItemById(item.id);
          }
        }
      ]
    });
    alert.present();

  }


  getTotal(): number {
    this.check = this.cartServ.getGrandTotal()
    return this.cartServ.getGrandTotal();
  }



  checkout(item) {

    let prod_details = this.cartList
    let orders = {};
    let new_list = [];

    for (let x in prod_details) {
      new_list.push({ 'product_id': prod_details[x]['product_id'], 'quantity': prod_details[x]['quantity'] })
    }

    orders = new_list

    console.log(new_list);
    console.log(JSON.stringify(orders));


    this.params = {
      "customer_id": 1,
      "orders": orders
    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();


    this.apis.process_order(this.params).then((result) => {


      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      this.orderJson = JSON.stringify(body1)
      this.order_id = body1['order_id']

      console.log('LETS SEE THE PROCESS ORDER ' + this.orderJson);
      console.log('LETS SEE THE ORDER ID IN PROCESS ORDER ' + this.order_id);
     
     
       this.params2 = {
      "order_id": this.order_id,

    }
    console.log("ORDER ID BEFOR PASSING IT " + this.params2)

    this.apis.display_orders(this.params2).then((result) => {
      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      this.orderJson = JSON.stringify(body1)

      console.log('LETS SEE THE DISPLAY ORDER JSON ' + this.orderJson);

      loader.dismiss();
       
            this.navCtrl.push(DeliveryDetailsPage,

              { value: this.orderJson });

    

    });

    });

   
    

    

  }




}
