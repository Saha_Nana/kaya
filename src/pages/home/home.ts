import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApisProvider } from '../../providers/apis/apis';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { GroceryPage } from '../../pages/grocery/grocery';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  from_login: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  raw: any;
  constructor(public navCtrl: NavController, public apis: ApisProvider, public loadingCtrl: LoadingController) {


      let loader = this.loadingCtrl.create({
      content: "Fetching Shops ..."

    });

    loader.present();
    this.apis.fetch_shops().then((result) => {

      console.log("RESULTS IS " + result);
      console.log("RESULTS IS" + this.apis.fetch_shops());
      var body = result["_body"];
      console.log("LETS SEE BODY IN HOME " + body);
      body = JSON.parse(body);
      this.check = body
      console.log("RESULTS IS " + this.check);
      this.body = Array.of(this.check)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

    }, (err) => {
      loader.dismiss();

      console.log(err);
    });


  }


  prod_type(shops) {



    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    console.log("SELECTED SHop IS " + shops.id)
      console.log("SELECTED SHop IS " + shops.name)

    let shop_id = shops.id
    let shop_name = shops.name

    setTimeout(() => {
      this.navCtrl.push(GroceryPage, { value: shop_id ,shop_name: shop_name });

    }, 3000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }



  refresh(){

      let loader = this.loadingCtrl.create({
      content: "Fetching Shops ..."

    });

    loader.present();
    this.apis.fetch_shops().then((result) => {

      console.log("RESULTS IS " + result);
      console.log("RESULTS IS" + this.apis.fetch_shops());
      var body = result["_body"];
      console.log("LETS SEE BODY IN HOME " + body);
      body = JSON.parse(body);
      this.check = body
      console.log("RESULTS IS " + this.check);
      this.body = Array.of(this.check)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

    }, (err) => {
      loader.dismiss();

      console.log(err);
    });


  }



}
