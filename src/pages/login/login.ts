

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { LocateMePage } from '../locate-me/locate-me';
import { Http } from '@angular/http';

import { FormBuilder, Validators } from '@angular/forms';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

   splash = true;

  public loginForm: any;
  submitAttempt: boolean = false;
  loginVal: any;
  jsonBody: any;
  jsonBody1: any;

  messageList: any;
  api_code: any;
  Phonenumber: string;
  PIN: string;
  retrieve: string;
  retrieve1: string;
  retrieve_pers: string;
  retrieve_doc: string;
  retrieve_doc3: string;

  searchbar: any;
  from_login: any = [];

  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  body1: any;
  person_type: any;
  person_type1: any;
  doctor_id: any;
  id: any;
  doc_id: any;
  doctor_id2: any;
  doc_params: any = [];
  doc_params2: any = [];

  // params: any = [];

  requester_id: any;
  data1: any = [];

  constructor(public toastCtrl: ToastController, public apis: ApisProvider, public _form: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: Http) {
      this.loginForm = this._form.group({

      "master_pin": ["", Validators.compose([Validators.required])],
      "mobile_number": ["", Validators.compose([Validators.required])]



    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  locate(){

    this.navCtrl.push(LocateMePage)


  }

  signup(){
this.navCtrl.push(SignupPage)

  }

login() {

    this.loginVal = JSON.stringify(this.loginForm.value);
    console.log("LETS SEE THE LOGIN VAL " + this.loginVal)

    this.jsonBody = JSON.parse(this.loginVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE SIGNUP VALUES STRINGIFY" + JSON.stringify(this.jsonBody))



    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    this.apis.login(this.jsonBody).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      loader.dismiss();
      if (this.api_code == "000") {
        let loader = this.loadingCtrl.create({
          content: "Login processing..."
        });

        loader.present();

          setTimeout(() => {
            this.navCtrl.setRoot(HomePage)

            

          }, 3000);

          setTimeout(() => {
            loader.dismiss();
          }, 3000);

        }

      

      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }




    },

      (err) => {

        let alert = this.alertCtrl.create({
          title: "",
          subTitle: "No internet connection",
          buttons: ['OK']
        });
        alert.present();

        this.toastCtrl.create({
          message: "Please check your internet connection",
          duration: 5000
        }).present();
        loader.dismiss();
        console.log(err);
      });
  }

}
