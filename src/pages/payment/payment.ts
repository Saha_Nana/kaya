import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MmoneyPage } from '../mmoney/mmoney';
import { VisaPage } from '../visa/visa';

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  order_json: any;
  list: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {


    this.order_json = this.navParams.get('value')
     console.log("order json " + this.order_json)  

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  check(){
    console.log("clcike")
  this.navCtrl.push(MmoneyPage)
    
}


  check2(){
    console.log("clcike")
  this.navCtrl.push(VisaPage)
    
  }

}
