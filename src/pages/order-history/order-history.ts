import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApisProvider, CartService, CartItem } from '../../providers/apis/apis';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { OrderModalPage } from '../order-modal/order-modal';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-order-history',
  templateUrl: 'order-history.html',
})
export class OrderHistoryPage {
  cartList: Array<CartItem>;
  check: any;
  params: any;
  params2: any;
  select: any;
  orders: any;
  orderJson: any;
  order_id: any;
  list: any;
  q: any;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public apis: ApisProvider, public cartServ: CartService, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

    this.params = {
      "customer_id": 1

    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();


    this.apis.order_history(this.params).then((result) => {


      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      

      this.orderJson = JSON.stringify(body1)
      this.order_id = body1['order_id']

      console.log('LETS SEE THE PROCESS ORDER ' + this.orderJson);  
      this.list = JSON.parse(this.orderJson)
      console.log('---------------------------------------'); 
       console.log('---------------------------------------'); 
        console.log('---------------------------------------'); 
       console.log('LETS SEE LIST PARSE ' + this.list); 

      loader.dismiss();

    });


  }


  details(q){

  //  this.list = JSON.stringify(list) 
  let select = q.order_id
    console.log("LETS SEE ID SELECTED " + select ) 
    

     let modal = this.modalCtrl.create(OrderModalPage, {

        value: select

      });

      modal.present();
    }
  

}
