import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { OrderHistoryPage } from '../order-history/order-history';


@Component({
  selector: 'page-payment-processed',
  templateUrl: 'payment-processed.html',
})
export class PaymentProcessedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentProcessedPage');
  }


  order_status(){

    this.navCtrl.push(OrderHistoryPage)

  }

  home(){

    this.navCtrl.setRoot(HomePage)
  }

}
