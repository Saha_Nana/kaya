import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';


@Component({
  selector: 'page-delivery-details',
  templateUrl: 'delivery-details.html',
})
export class DeliveryDetailsPage {
  order_json: any;
  list: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

     this.order_json = this.navParams.get('value')
     console.log("order json " + this.order_json)  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryDetailsPage');
  }

  submit(){

     this.navCtrl.push(PaymentPage,

              { value: this.order_json });
  }

}
