import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { LoginPage } from '../login/login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {

  public verifyForm: any;
  submitAttempt: boolean = false;

  messageList: any;
  from_signup: any;
  api_code: any;

  verifyVal: any;
  parse: any;
  parse1: any;
  jsonBody: any;
  jsonBody1: any;
  body: any;
  params: any = [];

  public itemList: Array<Object>;

  constructor(public navCtrl: NavController, public _form: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public apis: ApisProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

   

    this.from_signup = this.navParams.get('value')
    this.body = Array.of(this.from_signup)

    this.jsonBody = JSON.stringify(this.body);
    this.parse = JSON.parse(this.jsonBody);
  
    console.log('VALUE IN CONSTRUCTOR IS' + this.from_signup);
    console.log('VALUE IN stringiy IS' + this.jsonBody);
    console.log('VALUE IN json parse IS' + this.parse);

    this.verifyForm = this._form.group({
      "default_pin": [""],
      "master_pin": [""]


    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPage');
  }





  verify() {

 


     this.verifyVal = JSON.stringify(this.verifyForm.value);
    this.jsonBody1 = JSON.parse(this.verifyVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.verifyVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.parse[0].mobile_number)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
     console.log("LETS SEE default_pin " + this.jsonBody1.default_pin)
     console.log("LETS SEE master_pin " + this.jsonBody1.master_pin)
   
 

     this.params = {
      "mobile_number": this.parse[0].mobile_number,
      "default_pin": this.jsonBody1.default_pin,
      "master_pin": this.jsonBody1.master_pin


    }


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
    
    });

    loader.present();


    this.apis.verify(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let loader = this.loadingCtrl.create({
          content: "Verifying..."

        });
        loader.present();
        setTimeout(() => {
          this.navCtrl.setRoot(LoginPage,{ value: this.jsonBody });
        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

    }, (err) => {

      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();
      console.log(err);
    });
  }


}
