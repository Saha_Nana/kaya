import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import {VerifyPage} from '../verify/verify'
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
   public signupForm: any;
  submitAttempt: boolean = false;
  messageList: any;
  api_code: any;
  signupVal: any;
  jsonBody: any;

  public itemList: Array<Object>;


  constructor(public navCtrl: NavController, public apis: ApisProvider, public _form: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
  

   this.signupForm = this._form.group({
      "fullname": ["", Validators.compose([Validators.required])],
      "mobile_number": ["", Validators.compose([Validators.required])],
      "email": ["",Validators.compose([Validators.required])],
      "location": ["", Validators.compose([Validators.required])],
     


    })

}


 signup() {

    this.signupVal = JSON.stringify(this.signupForm.value);

    this.jsonBody = JSON.parse(this.signupVal);

    console.log("THIS IS THE SIGNUP raw values VALUES" + this.signupVal)
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)



    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
    });

    loader.present();

    this.apis.signup(this.jsonBody).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];

      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let loader = this.loadingCtrl.create({
          content: "Verifying Account ..."
          // duration: 5000


        });
        loader.present();



        setTimeout(() => {
          this.navCtrl.setRoot(VerifyPage, { value: this.jsonBody });
        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

       if (this.api_code == "105") {
        let loader = this.loadingCtrl.create({
          content: "Verifying Account ..."
          // duration: 5000


        });
        loader.present();



        setTimeout(() => {
          this.navCtrl.setRoot(VerifyPage, { value: this.jsonBody });
        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }





    }, (err) => {

      let alert = this.alertCtrl.create({
        title: "",
        subTitle: "No internet connection",
        buttons: ['OK']
      });
      alert.present();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();
      loader.dismiss();
      console.log(err);
    });
  }



  login(){

this.navCtrl.push(LoginPage)
  }

}
