import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApisProvider } from '../../providers/apis/apis';
import { ProdDetailsPage } from '../../pages/prod-details/prod-details';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-grocery',
  templateUrl: 'grocery.html',
})
export class GroceryPage {

  from_home: any;
  shop_name: any;
    messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  from_login: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  raw: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apis: ApisProvider, public loadingCtrl: LoadingController) {

    this.from_home = this.navParams.get('value')
    this.shop_name = this.navParams.get('shop_name')
    console.log("LETS SEE FROM HOME" +  this.from_home)
    console.log("SHOP NAME IS GROCERY" +  this.shop_name)


      this.raw = JSON.stringify(this.from_home);

    console.log('from ' +  this.from_home ); 
       console.log('RAW JSON' +  this.raw ); 
   
        
  
  this.params = {

   "shop_id": this.from_home

  }  
 console.log('PARAMETERS' +  this.params );

   this.apis.grocery_shops(this.raw).then((result) => {

    console.log("RESULT FROM API " + result);
    console.log("RESULTS IN PARAMS " + this.apis.grocery_shops(this.raw));
    var body = result["_body"];
    console.log("LETS SEE BODY IN HOME " + body);
    body = JSON.parse(body);
    this.check = body

    console.log("LETS SEE THE JSON OBJECT1" + body);
     console.log("LETS SEE THE JSON OBJECT" + this.check);
    this.body = Array.of(this.check)

    console.log("LETS SEE THE JSON ARRAY" + this.body);


    var desc = body["resp_desc"];
    var code = body["resp_code"];


    console.log(desc);
    console.log(code);

    this.messageList = desc;
    this.api_code = code;

    }, (err) => {


    console.log(err); 
  });



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroceryPage');
  }


  details(product){


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    console.log("SELECTED SHop IS " + product.id)

    let product_id = product.id
       let prod_name = product.name

    setTimeout(() => {
      this.navCtrl.push(ProdDetailsPage, { value: product_id, prod_name: product.name,shop_name: this.shop_name  });

    }, 3000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);


  }

}
