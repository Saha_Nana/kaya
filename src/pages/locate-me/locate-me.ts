import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Geolocation, Geoposition, Coordinates } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-locate-me',
  templateUrl: 'locate-me.html',
})
export class LocateMePage {

  resp: any;
  location: Coordinates;

  constructor(public toastCtrl: ToastController, private platform: Platform, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public nativeGeocoder: NativeGeocoder, public toaster: ToastController, private geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocateMePage');
  }

  getCurrentLocation() {
    let options = {
      enableHighAccuracy: true
    };
    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });
    loader.present();

    this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
      console.log("LOCATIONS FROM GEOLOACTION IS" + position)

      this.getcountry(position);
      console.log("CURRENT POSITIONIS " +   this.getcountry(position) )

      setTimeout(() => {
        this.navCtrl.setRoot(LoginPage)

      }, 3000);

      setTimeout(() => {
        loader.dismiss();
      }, 3000);
      // loader.dismiss();
      console.log("LOCATIONS FROM GEOLOACTION IS" + position)
    }).catch((err) => {
      alert(err);
    })

  }

  getcountry(pos) {
    this.nativeGeocoder.reverseGeocode(pos.coords.latitude, pos.coords.longitude).then((res: NativeGeocoderReverseResult) => {

      console.log("LETS SEE THE RESPONSE FROM REVERSE GEO CODE" + res)
      this.toastCtrl.create({
        message: "Location is : " + res.administrativeArea + res.subAdministrativeArea,
        duration: 5000
      }).present();

      let alert = this.alertCtrl.create({
        title: "",
        subTitle: res.countryName,
        buttons: ['OK']
      });
      alert.present();
    })
  }

  skip() {
    this.navCtrl.setRoot(LoginPage);

  }

    locate() {
    this.navCtrl.push(LoginPage);

  }

}

 
  //  
