import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PaymentProcessedPage } from '../payment-processed/payment-processed';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApisProvider, CartService, CartItem } from '../../providers/apis/apis';

@Component({
  selector: 'page-mmoney',
  templateUrl: 'mmoney.html',
})
export class MmoneyPage {
   cartList: Array<CartItem>;
  check: any;

  network: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public apis: ApisProvider, public cartServ: CartService) {
    console.log("LETS SEE network " + this.network)

}

 getTotal(): number {
    this.check = this.cartServ.getGrandTotal()
    return this.cartServ.getGrandTotal();
  }

  air(){
 console.log('AIR CHOSEN');
 this.network = "AIRTELTIGO"
 console.log("LETS SEE network " + this.network)

  }

  mtn(){
 console.log('MTN CHOSEN');
 this.network = "MTN"
 console.log("LETS SEE network " + this.network)

  }
    vod(){
 console.log('vod CHOSEN');
 this.network = "VODAFONE"
 console.log("LETS SEE network " + this.network)

  }
  

  pay(){

    this.navCtrl.setRoot(PaymentProcessedPage)
  }

}
