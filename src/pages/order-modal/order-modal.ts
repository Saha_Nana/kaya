import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';


@Component({
  selector: 'page-order-modal',
  templateUrl: 'order-modal.html',
})
export class OrderModalPage {
 
  check: any;
  params: any;
  params2: any;
  select: any;
  orders: any;
  orderJson: any;
  order_id: any;
  list: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public apis: ApisProvider,public loadingCtrl: LoadingController, public alertCtrl: AlertController,public viewCtrl: ViewController) {

    this.orderJson = this.navParams.get('value')
    console.log("ORDER ID IN MODAL " +  this.orderJson )

     this.params = {
      
      "order_id": this.orderJson
    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();


    this.apis.display_orders(this.params).then((result) => {


      var body1 = result["_body"];
      body1 = JSON.parse(body1);
       this.list = Array.of(body1)

      this.orderJson = JSON.stringify(body1)
      this.order_id = body1['order_id']
      

      console.log('LETS SEE THE PROCESS ORDER ' + this.orderJson);
      console.log('LETS SEE THE ORDER ID IN PROCESS ORDER ' + this.order_id);

      loader.dismiss();
       
            // this.navCtrl.push(DeliveryDetailsPage,

            //   { value: this.orderJson });

    

    });

  }

   closeModal() {


    this.viewCtrl.dismiss();
  }
}
